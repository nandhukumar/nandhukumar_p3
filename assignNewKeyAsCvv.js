// 3. Assign a new field to each card for CVV where the CVV is a random 3 digit number.
// 4. Add a new field to each card to indicate if the card is valid or not.
// 5. Invalidate all cards issued before March.
// 6. Sort the data into ascending order of issue date.
// 7. Group the data in such a way that we can identify which cards were assigned in which months.

// {
//     id: 10,
//     card_number: "4026933464803521",
//     card_type: "visa-electron",
//     issue_date: "10/1/2021",
//     salt: "cAsGiHMFTPU",
//     phone: "372-887-5974",
//   }

function sumOfOddNumbersInEvenPosition(cardData) {
  let sortingEachDataByMonth = cardData
    .map((eachData) => {
      let cvvNum = Math.ceil(Math.random() * 1000);
      eachData["CVV"] = cvvNum;
      return eachData;
    })
    .map((eachData) => {
      eachData["is_valid"] = true;
      return eachData;
    })
    .map((eachData) => {
      let issueDate = eachData.issue_date;
      let date = new Date(issueDate);
      let monthOfIssueDate = date.getMonth();
      if (monthOfIssueDate < 2) {
        eachData["is_valid"] = false;
      }
      return eachData;
    })
    .sort((data1, data2) => {
      let issueDateOfData1 = new Date(data1.issue_date);
      let issueDateOfData2 = new Date(data2.issue_date);

      if (issueDateOfData1 > issueDateOfData2) {
        return 1;
      } else {
        return -1;
      }
    })
    .reduce((acc, currentVal) => {
      let allMonthsInArray = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
      ];

      let issueDate = currentVal.issue_date;
      let date = new Date(issueDate);
      let monthOfIssueDate = date.getMonth();
      let issueMonth = allMonthsInArray[monthOfIssueDate];

      if (!(issueMonth in acc)) {
        acc[issueMonth] = [];
      }
      acc[issueMonth].push(currentVal);
      return acc;
    }, {});

  return sortingEachDataByMonth;
}

module.exports = sumOfOddNumbersInEvenPosition;
