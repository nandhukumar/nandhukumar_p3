const fs = require('fs');
const path = require('path');

let cardData = require("../p3");

let assignNewKeyAsCvv = require("../assignNewKeyAsCvv");

let result = assignNewKeyAsCvv(cardData);

fs.writeFile(path.join(__dirname, "output.json"), JSON.stringify(result), (error) => {
    if(error) {
        return console.log(error);
    }
    console.log("File written successfully");
})
