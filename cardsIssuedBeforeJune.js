// 2. Find all cards that were issued before June.

// {
//     id: 10,
//     card_number: "4026933464803521",
//     card_type: "visa-electron",
//     issue_date: "10/1/2021",
//     salt: "cAsGiHMFTPU",
//     phone: "372-887-5974",
//   }

function cardsIssuedBeforeJune(cardData) {
  let filteredCardData = cardData.filter((eachData) => {
    let issueDate = eachData.issue_date;
    let date = new Date(issueDate);
    let monthOfIssueDate = date.getMonth();
    if (monthOfIssueDate < 5) {
      return eachData;
    }
  });
  return filteredCardData;
}

module.exports = cardsIssuedBeforeJune;
