// 1. Find all card numbers whose sum of all the even position digits is odd.

// {
//     id: 10,
//     card_number: "4026933464803521",
//     card_type: "visa-electron",
//     issue_date: "10/1/2021",
//     salt: "cAsGiHMFTPU",
//     phone: "372-887-5974",
//   }

function sumOfOddNumbersInEvenPosition(cardData) {
  let filteredData = cardData.filter((eachData) => {
    let splittedCardNum = eachData.card_number.split("");

    let sumOfEvenPosition = splittedCardNum.reduce((acc, currentVal, index) => {
      if (index % 2 === 0) {
        acc += parseInt(currentVal);
      }
      return acc;
    }, 0);

    if (sumOfEvenPosition % 2 !== 0) {
      return eachData;
    }
  });
  return filteredData;
}

module.exports = sumOfOddNumbersInEvenPosition;
